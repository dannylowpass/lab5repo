/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;


public class Airplane {
    
    private String make;
    private String model;
    
    public Airplane(String make, String model){
        this. make = make;
        this.model = model;
    }
    
    public String getMake(){
        return this.make;
    }
    
    public String getModel(){
        return this.model;
    }
}
