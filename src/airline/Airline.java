/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;


public class Airline {
  
    private String name;
    private String shortCode;
    
    public Airline(String name, String shortCode){
      this.name = name;
      this.shortCode = shortCode;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getShortcode(){
        return this.shortCode;
    }
}



