/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;
import java.util.Date;


public class Airport {
   
    private String code;
    private String city;
    
    public Airport(String code, String city){
        this.code = code;
        this.city = city;
    }
    
    public String getCode(){
        return this.code;
    }
    
    public String getCity(){
        return this.city;
    }
}
