/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline;
import java.util.Date;


public class AirlineAssociation {

    
    public static void main(String[] args){

    Airline airline1 = new Airline("AirCanada", "AC");
    Airport airport1 = new Airport("YYZ", "Toronto");
    Airplane airplane1 = new Airplane("Boeing", "747");
    Flight flight1 = new Flight("YYZ0001", new Date (21, 06, 16, 18, 00, 00));
    
    System.out.println("Flight " + flight1.getCode() + " From airline " +
        airline1.getName() + " uses a " + airplane1.getMake() + 
        airplane1.getModel() + " leaving from airport " + airport1.getCode());    
   
    }
}
